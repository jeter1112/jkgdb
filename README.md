# KGDB Tutorial
----
### Coming Up
* Introduction to kernel debugging
  * KGDB
  * QEmu/KVM/UML
  * JTAG
* Preparation
  * Virtual target machine install
  * Compile target linux kernel with -g parameter
  * Connect host & target with serial port
  * Configure grub and Vmware
  * Host run gdb to debug target
* Debug example
  * Set breakpoint
  * More instructions
---
## Introduction to kernel debugging

> The philosophy of Debugging Linux kernel seems like debugging apps. For debugging an app, we need compile the program with -g parameter, which is similar to debugging kernel with compiling the whole kernel source with some kgdb parameters. Then running gdb on another machine(host) to debug this compiled kernel target machine. There ara three types of debugging platforms. The differences are listed here.

##### KGDB
![](/Images/KGDB.png)
##### Qemu/KVM/UML
![](/Images/QEMU.png)
##### JTAG
![](/Images/Jtag.png)

--------


## Preparation
#### Virtual target machine install
Here we just use VMware to install deepin 15.7 virtual machine as the target machine.
(Hint: Do better allocate more than 40GB memory to the machine, because In the compling phase will produce more than 11GB)

#### Compile target machine with -g parameter


1. For dependencies...
```sh
$ sudo apt install build-essential kernel-package libncurses5-dev fakeroot bison flex libssl-dev qt5-default quilt
```
2. Then follows 2 phases: 
- Download the deepin source;
- compile the source with KGDB parameters;

```sh

$ sudo apt install linux-source-deepin
$ cd linux.... (enter the linux source directory)
$ make menuconfig 
(In default, KGDB parameters have been selected, just selects "save" button save the result to the .config )
$ su root
$ make -jN && make -jN modules && make modules_install
```
( Notice! compile time may exceed 4 hours. )

3. copy "vmlinux" file to the host machine( vmlinux is used in host gdb debug phase ).
#### Connect host & target with serial port
This part makes sure host & target can communicate.

For target machine setting:
1. shutdown the machine.
2. In Vmware hardware setting, add serial Port, select "Connect at power on" and "Yield CPU on poll". Then select "Use socket": "/tmp/vmpipe", From "Server" To "An Application". ("vmpipe" can be any other name, host machine should use the same name later.)

![](/Images/vmwaresetting.png)


For host machine setting:
```sh
$ sudo apt install socat
```

Now, check the communication:
In host:
1. open the terminal:
    ```sh
    $ socat -d -d /tmp/vmpipe tcp-listen:9001
    ```
2. open another terminal:
    ```sh
    $ telnet 127.0.0.1 9001
    ```

In target:
1. open the machine
2. in terminal:
```sh
$ su root
$ echo whatevewords /dev/ttyS1
```
If the console of "telnet 127.0.0.1" in host machine output the "whatevewords", then communication suceed.
![](Images/communicate.png)

#### Configure grub
In target machine:
1. In the directory of compling the kernel section:
    ```sh
    $ sudo make install
    ```
    ( Notice! wait until the shell prompt shows up.Ignore modules loading errors here.)
    ![](Images/makeinstall.png)
2. go to the /boot/grub, open the grub.cfg file,
    add "kgdbwait kgdboc=ttyS1,115200 nokaslr" after the "linux  /boot/vmlinuz-4.16.12 ......splash quiet"
    ( kaslr represents "KERNEL ADDRESS SPACE RANDOMIZATION", if not turn off kaslr, gdb cannot set breakpoints,find address... ) 
    ![](/Images/grubcfg.png)
3. go to the /etc/default, open the grub file,
    TIMEOUT=0
4. shutdown the target, open again, the machine will wait for remote gdb
    ![](Images/targetkernelstart.png)
#### Host run gdb to debug target
In host:
1. open the socat:
    ```sh
    $ socat -d -d /tmp/vmpipe tcp-listen:9001
    ```
2. go to the vmlinux file(In comling section, copied):
    ```sh
    $ gdb ./vmlinux
    ...
    (gdb) set serial baud 115200
    (gdb) target remote localhost:9001
    ```
3. OK to hack.
   ![](Images/gdbsetting.png)
-----
## Debug example
Refer to https://tthtlc.wordpress.com/2012/06/16/virtualbox-kgdb-analysis-of-linux-kernel-v3-4-0-rc3/
![](/Images/debugexample.png)
Other references:
-----
http://freemandealer.github.io/2015/03/18/kernel-debugging/
http://blog.dynofu.me/post/2015/10/08/linux-live-debugging-with-kgdb-vmware-workstion.html